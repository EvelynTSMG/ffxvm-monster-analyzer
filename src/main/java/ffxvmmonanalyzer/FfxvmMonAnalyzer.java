/* ###
 * IP: GHIDRA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ffxvmmonanalyzer;

import ghidra.app.cmd.label.AddLabelCmd;
import ghidra.app.services.AbstractAnalyzer;
import ghidra.app.services.AnalyzerType;
import ghidra.app.util.importer.MessageLog;
import ghidra.framework.options.Options;
import ghidra.program.flatapi.FlatProgramAPI;
import ghidra.program.model.address.Address;
import ghidra.program.model.address.AddressSetView;
import ghidra.program.model.data.CategoryPath;
import ghidra.program.model.data.DataType;
import ghidra.program.model.data.DataTypeManager;
import ghidra.program.model.data.BooleanDataType;
import ghidra.program.model.data.ByteDataType;
import ghidra.program.model.data.IntegerDataType;
import ghidra.program.model.data.InvalidDataTypeException;
import ghidra.program.model.data.ShortDataType;
import ghidra.program.model.data.StructureDataType;
import ghidra.program.model.listing.Program;
import ghidra.program.model.mem.Memory;
import ghidra.program.model.symbol.SourceType;
import ghidra.util.exception.CancelledException;
import ghidra.util.task.TaskMonitor;

import java.io.ByteArrayInputStream;

import com.google.common.io.LittleEndianDataInputStream;

import ffxvmmonanalyzer.struct.ScriptObject.ScriptHeader;
import ffxvmmonanalyzer.struct.monster.MonsterStatDataObject;

/**
 * TODO: Provide class-level documentation that describes what this analyzer does.
 */
public class FfxvmMonAnalyzer extends AbstractAnalyzer {
	Address start;
	Memory mem;
	FlatProgramAPI fpapi;
	Address absStartAddr;
	Address scriptStartAddr;
	Address unkChunkAddr;
	Address valsStartAddr;
	Address nullAddr1;
	Address lootStartAddr;
	Address lootEndAddr;
	Address textAddr;
	int filesize;
	ScriptHeader header;
	StructureDataType monStats;
	
	public FfxvmMonAnalyzer() {

		// TODO: Name the analyzer and give it a description.

		super("FFXVM Monster Analyzer", "Analyzes FFX Monster files. Will fail to analyze other similar files.",
				AnalyzerType.BYTE_ANALYZER);
	}

	@Override
	public boolean getDefaultEnablement(Program program) {

		// TODO: Return true if analyzer should be enabled by default

		return false;
	}

	@Override
	public boolean canAnalyze(Program program) {

		// TODO: Examine 'program' to determine of this analyzer should analyze it.  Return true
		// if it can.

		return true; // Pray
	}

	@Override
	public void registerOptions(Options options, Program program) {

		// TODO: If this analyzer has custom options, register them here

		//options.registerOption("Option name goes here", false, null,
		//	"Option description goes here");
	}

	@Override
	public boolean added(Program program, AddressSetView set, TaskMonitor monitor, MessageLog log)
			throws CancelledException {

		// TODO: Perform analysis when things get added to the 'program'.  Return true if the
		// analysis succeeded.
		
		start = program.getAddressFactory().getDefaultAddressSpace().getAddress(0);
		mem = program.getMemory();
		fpapi = new FlatProgramAPI(program);
		
		try {
			addCustomStructs(program, log);
		} catch (Exception ex) {
			log.appendException(ex);
			return false;
		}
			
		analyzeFileHeader(program, log);
		
		Address nextChunkAfterScript = unkChunkAddr.compareTo(fpapi.toAddr(0)) == 1 ? unkChunkAddr : valsStartAddr;
		int scriptLength = (int)nextChunkAfterScript.getOffset() - (int)scriptStartAddr.getOffset();
		
		//analyzeScriptHeader(program, log);
		
		try {
			fpapi.createLabel(valsStartAddr, "monStats", true, SourceType.ANALYSIS);
			fpapi.createData(valsStartAddr, monStats);
		} catch (Exception ex) {
			log.appendException(ex);
			return false;
		}
		
		
		return true;
	}
	
	public boolean addCustomStructs(Program program, MessageLog log)
			throws InvalidDataTypeException {
		DataTypeManager dtm = program.getDataTypeManager();

		DataType dataBool = new BooleanDataType();
		DataType dataByte = new ByteDataType();
		DataType dataShort = new ShortDataType();
		DataType dataInt = new IntegerDataType();
		
		monStats = new StructureDataType(new CategoryPath("/ChunkTypes"), "monStats", 0x80, dtm);
		monStats.insertAtOffset(0x14, dataInt, 0x4, "hp", "Max HP");
		monStats.insertAtOffset(0x18, dataInt, 0x4, "mp", "Max MP");
		monStats.insertAtOffset(0x1C, dataInt, 0x4, "overkill", "Overkill Threshold");
		
		monStats.insertAtOffset(0x20, dataByte, 0x1, "str", "Strength");
		monStats.insertAtOffset(0x21, dataByte, 0x1, "def", "Defense");
		monStats.insertAtOffset(0x22, dataByte, 0x1, "mag", "Magic");
		monStats.insertAtOffset(0x23, dataByte, 0x1, "mdf", "Magic Defense");
		monStats.insertAtOffset(0x24, dataByte, 0x1, "agi", "Agility");
		monStats.insertAtOffset(0x25, dataByte, 0x1, "lck", "Luck");
		monStats.insertAtOffset(0x26, dataByte, 0x1, "eva", "Evasion");
		monStats.insertAtOffset(0x27, dataByte, 0x1, "acc", "Accuracy");
		
		monStats.insertAtOffset(0x28, dataByte, 0x1, "miscProp28", "Some miscellaneous properties");
		monStats.insertAtOffset(0x29, dataByte, 0x1, "miscProp29", "Some more miscellaneous properties");
		monStats.insertAtOffset(0x2a, dataByte, 0x1, "poisonDmg", "Poison Damage (% of Max HP * 100)");
		
		monStats.insertBitFieldAt(0x2b, 0x1, 0, dataByte, 5, "absorbFlags", "Whether monster absorbs a certain elements");
		monStats.insertBitFieldAt(0x2c, 0x1, 0, dataByte, 5, "nulFlags", "Whether monster is immune to certain elements");
		monStats.insertBitFieldAt(0x2d, 0x1, 0, dataByte, 5, "resistFlags", "Whether monster resists a certain elements");
		monStats.insertBitFieldAt(0x2e, 0x1, 0, dataByte, 5, "weakFlags", "Whether monster is weak to certain elements");
		
		monStats.insertAtOffset(0x2f, dataByte, 0x1, "resistDeath", "Resistance to Death (% * 100, 255==immune)");
		monStats.insertAtOffset(0x30, dataByte, 0x1, "resistZombie", "Resistance to Zombie (% * 100, 255==immune)");
		monStats.insertAtOffset(0x31, dataByte, 0x1, "resistStone", "Resistance to Petrification (% * 100, 255==immune)");
		monStats.insertAtOffset(0x32, dataByte, 0x1, "resistPoison", "Resistance to Poison (% * 100, 255==immune)");
		monStats.insertAtOffset(0x33, dataByte, 0x1, "resistPowerBreak", "Resistance to Power Break (% * 100, 255==immune)");
		monStats.insertAtOffset(0x34, dataByte, 0x1, "resistMagicBreak", "Resistance to Magic Break (% * 100, 255==immune)");
		monStats.insertAtOffset(0x35, dataByte, 0x1, "resistArmorBreak", "Resistance to Armor Break (% * 100, 255==immune)");
		monStats.insertAtOffset(0x36, dataByte, 0x1, "resistMentalBreak", "Resistance to Mental Break (% * 100, 255==immune)");
		monStats.insertAtOffset(0x37, dataByte, 0x1, "resistConfuse", "Resistance to Confused (% * 100, 255==immune)");
		monStats.insertAtOffset(0x38, dataByte, 0x1, "resistBerserk", "Resistance to Berserk (% * 100, 255==immune)");
		monStats.insertAtOffset(0x39, dataByte, 0x1, "resistProvoke", "Resistance to Provoke (% * 100, 255==immune)");
		monStats.insertAtOffset(0x3a, dataByte, 0x1, "chanceThreaten", "Chance to Threaten (% * 100, 0==immune)");
		monStats.insertAtOffset(0x3b, dataByte, 0x1, "resistSleep", "Resistance to Sleep (% * 100, 255==immune)");
		monStats.insertAtOffset(0x3c, dataByte, 0x1, "resistSilence", "Resistance to Silence (% * 100, 255==immune)");
		monStats.insertAtOffset(0x3d, dataByte, 0x1, "resistDarkness", "Resistance to Darkness (% * 100, 255==immune)");
		monStats.insertAtOffset(0x3e, dataByte, 0x1, "resistShell", "Resistance to Shell (% * 100, 255==immune)");
		monStats.insertAtOffset(0x3f, dataByte, 0x1, "resistProtect", "Resistance to Protect (% * 100, 255==immune)");
		monStats.insertAtOffset(0x40, dataByte, 0x1, "resistReflect", "Resistance to Reflect (% * 100, 255==immune)");
		monStats.insertAtOffset(0x41, dataByte, 0x1, "resistRegen", "Resistance to Regen (% * 100, 255==immune)");
		monStats.insertAtOffset(0x42, dataByte, 0x1, "resistNulFire", "Resistance to NulBlaze (% * 100, 255==immune)");
		monStats.insertAtOffset(0x43, dataByte, 0x1, "resistNulIce", "Resistance to NulFrost (% * 100, 255==immune)");
		monStats.insertAtOffset(0x44, dataByte, 0x1, "resistNulThunder", "Resistance to NulShock (% * 100, 255==immune)");
		monStats.insertAtOffset(0x45, dataByte, 0x1, "resistNulWater", "Resistance to NulTide (% * 100, 255==immune)");
		monStats.insertAtOffset(0x46, dataByte, 0x1, "resistHaste", "Resistance to Haste (% * 100, 255==immune)");
		monStats.insertAtOffset(0x47, dataByte, 0x1, "resistSlow", "Resistance to Slow (% * 100, 255==immune)");
		
		monStats.insertAtOffset(0x48, dataInt, 0x4, "autoStatusFlags", "Whether monster has a status auto-applied");
		monStats.insertAtOffset(0x4e, dataShort, 0x2, "extraStatusImmunities", "");
		
		monStats.insertAtOffset(0x70, dataByte, 0x1, "forcedAction", "");
		
		
		return true;
	}
	
	public boolean analyzeFileHeader(Program program, MessageLog log) {
		byte[] headerBytes = new byte[40];
		
		try {
			mem.getBytes(start, headerBytes);
			LittleEndianDataInputStream headerStream = new LittleEndianDataInputStream(new ByteArrayInputStream(headerBytes));
			
			// Fill in stuffs
			absStartAddr = fpapi.toAddr(headerStream.readInt());
			scriptStartAddr = fpapi.toAddr(headerStream.readInt());
			unkChunkAddr = fpapi.toAddr(headerStream.readInt());
			valsStartAddr = fpapi.toAddr(headerStream.readInt());
			nullAddr1 = fpapi.toAddr(headerStream.readInt());
			lootStartAddr = fpapi.toAddr(headerStream.readInt());
			lootEndAddr = fpapi.toAddr(headerStream.readInt());
			textAddr = fpapi.toAddr(headerStream.readInt());
			filesize = headerStream.readInt();
			
			// Labels
			fpapi.createLabel(start, "absStartAddr", true, SourceType.ANALYSIS);
			fpapi.createLabel(start.add(0x4), "scriptStartAddr", true, SourceType.ANALYSIS);
			fpapi.createLabel(start.add(0x8), "unkChunkAddr", true, SourceType.ANALYSIS);
			fpapi.createLabel(start.add(0xC), "valsStartAddr", true, SourceType.ANALYSIS);
			fpapi.createLabel(start.add(0x10), "nullAddr1", true, SourceType.ANALYSIS);
			fpapi.createLabel(start.add(0x14), "lootStartAddr", true, SourceType.ANALYSIS);
			fpapi.createLabel(start.add(0x18), "lootEndAddr", true, SourceType.ANALYSIS);
			fpapi.createLabel(start.add(0x1C), "textAddr", true, SourceType.ANALYSIS);
			fpapi.createLabel(start.add(0x20), "filesize", true, SourceType.ANALYSIS);
			
			// Data
			DataType dataInt = new IntegerDataType();
			
			fpapi.createData(start, dataInt);
			fpapi.createData(start.add(0x4), dataInt);
			fpapi.createData(start.add(0x8), dataInt);
			fpapi.createData(start.add(0xC), dataInt);
			fpapi.createData(start.add(0x10), dataInt);
			fpapi.createData(start.add(0x14), dataInt);
			fpapi.createData(start.add(0x18), dataInt);
			fpapi.createData(start.add(0x1C), dataInt);
			fpapi.createData(start.add(0x20), dataInt);
			
		} catch (Exception ex) {
			log.appendException(ex);
		}
		
		return true;
	}
	
	public boolean analyzeScriptHeader(Program program, MessageLog log) {
		byte[] headerBytes = new byte[52];
		
		try {
			mem.getBytes(scriptStartAddr, headerBytes);
			LittleEndianDataInputStream headerStream = new LittleEndianDataInputStream(new ByteArrayInputStream(headerBytes));
			
			// Fill in the header
			header = new ScriptHeader();
			header.count1 = headerStream.readUnsignedShort();
			header.someRefsCount = headerStream.readUnsignedShort();
			header.refIntCount = headerStream.readUnsignedShort();
			header.refFloatCount = headerStream.readUnsignedShort();
			header.entryPointCount = headerStream.readUnsignedShort();
			header.maybeJumpCount = headerStream.readUnsignedShort();
			header.alwaysZero1 = headerStream.readInt();
			header.alwaysZero2 = headerStream.readInt();
			header.someRefsOffset = headerStream.readInt();
			header.intTableOffset = headerStream.readInt();
			header.floatTableOffset = headerStream.readInt();
			header.scriptEntryPointsOffset = headerStream.readInt();
			header.jumpsOffset = headerStream.readInt();
			header.alwaysZero3 = headerStream.readInt();
			header.alwaysZero4 = headerStream.readInt();
			header.weirdoOffset = headerStream.readInt();
			
			// Create labels
			fpapi.createLabel(scriptStartAddr, "count1", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x2), "someRefsCount", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x4), "refIntCount", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x6), "refFloatCount", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x8), "entryPointCount", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0xA), "maybeJumpCount", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0xC), "alwaysZero1", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x10), "alwaysZero2", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x14), "someRefsOffset", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x18), "intTableOffset", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x1C), "floatTableOffset", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x20), "scriptEntryPointsOffset", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x24), "jumpsOffset", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x28), "alwaysZero3", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x2C), "alwaysZero4", true, SourceType.ANALYSIS);
			fpapi.createLabel(scriptStartAddr.add(0x30), "weirdoOffset", true, SourceType.ANALYSIS);
			
			// Create data
			DataType dataShort = new ShortDataType();
			DataType dataInt = new IntegerDataType();
			
			fpapi.createData(scriptStartAddr, dataShort);
			fpapi.createData(scriptStartAddr.add(0x2), dataShort);
			fpapi.createData(scriptStartAddr.add(0x4), dataShort);
			fpapi.createData(scriptStartAddr.add(0x6), dataShort);
			fpapi.createData(scriptStartAddr.add(0x8), dataShort);
			fpapi.createData(scriptStartAddr.add(0xA), dataShort);
			fpapi.createData(scriptStartAddr.add(0xC), dataInt);
			fpapi.createData(scriptStartAddr.add(0x10), dataInt);
			fpapi.createData(scriptStartAddr.add(0x14), dataInt);
			fpapi.createData(scriptStartAddr.add(0x18), dataInt);
			fpapi.createData(scriptStartAddr.add(0x1C), dataInt);
			fpapi.createData(scriptStartAddr.add(0x20), dataInt);
			fpapi.createData(scriptStartAddr.add(0x24), dataInt);
			fpapi.createData(scriptStartAddr.add(0x28), dataInt);
			fpapi.createData(scriptStartAddr.add(0x2C), dataInt);
			fpapi.createData(scriptStartAddr.add(0x30), dataInt);
			
		} catch (Exception ex) {
			log.appendException(ex);
			return false;
		}
		
		return true;
	}
}
