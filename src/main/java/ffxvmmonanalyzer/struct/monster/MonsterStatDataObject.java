package ffxvmmonanalyzer.struct.monster;

public class MonsterStatDataObject {
    public int hp;
    public int mp;
    public int overkill;
    public byte str;
    public byte def;
    public byte mag;
    public byte mdf;
    public byte agi;
    public byte lck;
    public byte eva;
    public byte acc;
    public int miscProp28;
    public int miscProp29;
    public byte poisonDamage;
    public byte absorbFlags;
    public byte nulFlags;
    public byte resistFlags;
    public byte weakFlags;
    public byte resistDeath;
    public byte resistZombie;
    public byte resistStone;
    public byte resistPoison;
    public byte resistPowerBreak;
    public byte resistMagicBreak;
    public byte resistArmorBreak;
    public byte resistMentalBreak;
    public byte resistConfuse;
    public byte resistBerserk;
    public byte resistProvoke;
    public byte chanceThreaten;
    public byte resistSleep;
    public byte resistSilence;
    public byte resistDarkness;
    public byte resistShell;
    public byte resistProtect;
    public byte resistReflect;
    public byte resistRegen;
    public byte resistNulFire;
    public byte resistNulIce;
    public byte resistNulThunder;
    public byte resistNulWater;
    public byte resistHaste;
    public byte resistSlow;
    public int autoStatusFlags;

    public short extraStatusImmunities;

    public byte forcedAction;

    public MonsterStatDataObject() {}
}
