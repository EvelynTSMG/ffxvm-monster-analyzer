# FFXVM Monster Analyzer

This Ghidra extension aims to analyze and properly label Final Fantasy X's monster files, as well as decompile the scripts they contain.

## How to contribute
PR